<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Global Enumerations
    |--------------------------------------------------------------------------
    | This file contains the different enumerations to provide an easy way
    | to change them later on.
    |
    */
    'payment_methods' => ['CASH', 'CARD'],
    'payment_methods_default' => 'CASH',
    'order_statuses' => ['CART', 'RECEIVED', 'REJECTED', 'ACCEPTED'],
];
