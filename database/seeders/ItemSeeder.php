<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Item;
use App\Models\Category;

class ItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Item::factory(10)->create()->map(function ($item) {
            $category_ids = Category::all()->random(rand(1,3))->pluck('id')->toArray();
            $item->categories()->attach($category_ids);
        });
    }
}
