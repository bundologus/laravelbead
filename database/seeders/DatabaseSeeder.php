<?php

namespace Database\Seeders;

use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use App\Models\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->truncate();
        DB::table('categories')->truncate();
        DB::table('items')->truncate();
        Storage::delete(Storage::files('public/images/items'));
        DB::table('orders')->truncate();
        DB::table('ordered_items')->truncate();

        User::create([
            'name' => 'admin',
            'email' => 'admin@szerveroldali.hu',
            'email_verified_at' => Carbon::now(),
            'password' => Hash::make('password'),
            'is_admin' => true,
            'remember_token' => Str::random(10),
        ]);

        $users = ['user1', 'user2', 'user3'];

        foreach ($users as $user) {
            User::create([
                'name' => $user,
                'email' => $user . '@szerveroldali.hu',
                'email_verified_at' => Carbon::now(),
                'password' => Hash::make('password'),
                'is_admin' => false,
                'remember_token' => Str::random(10),
            ]);
        }

        $this->call([
            CategorySeeder::class,
            ItemSeeder::class,
            OrderSeeder::class,
            OrderedItemSeeder::class,
        ]);
    }
}
