<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Category;
use Faker\Generator;
use Illuminate\Container\Container;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Container::getInstance()->make(Generator::class);
        $names = $faker->words(7, false);
        foreach($names as $name) {
            Category::insert(['name' => $name]);
        }
    }
}
