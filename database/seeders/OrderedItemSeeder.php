<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\OrderedItem;
use App\Models\Order;

class OrderedItemSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $orders = Order::pluck('id')->toArray();

        foreach ($orders as $order) {
            OrderedItem::factory(rand(1,3))->create(['order_id' => $order]);
        }
    }
}
