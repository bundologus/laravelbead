<?php

namespace Database\Factories;

use App\Models\Item;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Facades\Storage;

class ItemFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Item::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->words(rand(1,3), true),
            'description' => $this->faker->paragraph(3, true),
            'price' => rand(1,10000) + rand(1,100)/100,
            'image_url' => (rand(0,1)) ?
                $this->faker->image(Storage::path('public') . '/images/items', 300,200, 'technics', false, false) :
                (null),
        ];
    }
}
