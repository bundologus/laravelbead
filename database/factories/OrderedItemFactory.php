<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\OrderedItem;
use App\Models\Item;

class OrderedItemFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = OrderedItem::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $items = Item::pluck('id')->toArray();
        return [
            'item_id' => $this->faker->randomElement($items),
            'quantity' => rand(1,6),
        ];
    }
}
