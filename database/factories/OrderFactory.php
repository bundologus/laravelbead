<?php

namespace Database\Factories;

use App\Models\Order;
use Illuminate\Database\Eloquent\Factories\Factory;
use App\Models\User;

class OrderFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Order::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $user_id = $this->faker->randomElement(User::pluck('id')->toArray());
        $user = User::find($user_id);
        $payment_methods = config("enums.payment_methods");
        $order_statuses = config("enums.order_statuses");
        if ( count($user->orders->where('status','CART')->all()) > 0 ) {
            $order_statuses = array_filter($order_statuses, function ($item) { return $item != 'CART';});
        }
        return [
            "user_id" => $user_id,
            "address" => (rand(0,1)) ? ($this->faker->address()) : (null),
            "comment" => (rand(0,1)) ? ($this->faker->words(rand(5,10), true)) : (null),
            "payment_method" => $payment_methods[rand(0,1)],
            "status" => $this->faker->randomElement($order_statuses),
            "received_on" => $this->faker->dateTime('now','Europe/Budapest')
        ];
    }
}
