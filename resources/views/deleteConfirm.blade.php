@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12 col-md-3"></div>
        <div class="col-12 col-md-6">
            @if($type == "category")
            <div class="card">
                <div class="card-header text-white bg-danger"><p class="h4 mb-0">Warning, deleting category {{$obj->name}}!</p></div>
                <div class="card-body">
                    <form method="POST" action="{{ route('delete.category') }}">
                        @csrf
                        <input type="hidden" id="id" name="id" value="{{$id}}" readonly>
                        <p class="text-danger">This operation is <u>not</u> reversible.</p>
                        <div class="form-row">
                            <div class="col-6">
                                <button type="submit" class="btn btn-block btn-danger">Delete</button>
                            </div>
                            <div class="col-6">
                                <a role="button" class="btn btn-block btn-secondary" href="{{ route('manage.categories') }}">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @endif
            @if($type == "item")
            <div class="card">
                <div class="card-header text-white bg-danger"><p class="h4 mb-0">Warning, deleting item {{$obj->name}}!</p></div>
                <div class="card-body">
                    <form method="POST" action="{{ route('delete.item') }}">
                        @csrf
                        <input type="hidden" id="id" name="id" value="{{$id}}" readonly>
                        <p>This operation is reversible via the item administration page.</p>
                        <div class="form-row">
                            <div class="col-6">
                                <button type="submit" class="btn btn-block btn-danger">Delete</button>
                            </div>
                            <div class="col-6">
                                <a role="button" class="btn btn-block btn-secondary" href="{{ route('manage.items') }}">Cancel</a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            @endif
        </div>
        <div class="col-12 col-md-3"></div>
    </div>
</div>
@endsection
