@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <h1>@if ($item == null) New item @else Editing item @endif</h1>
        <form
            class="col-12"
            method="POST"
            @if($item == null)
                action="{{ route('store.item') }}"
            @else
                action="{{ route('update.item', ['id' => $item->id]) }}"
            @endif
            enctype="multipart/form-data">
            @csrf
            <input type="hidden" id="id" name="id" value="@if ($item != null) {{$item->id}} @endif" readonly>
            <div class="form-row">
                <div class="form-group col-12 col-sm-9">
                    <label for="name">Item name</label>
                    <input
                        id="name"
                        name="name"
                        type="text"
                        class="form-control @error('name') is-invalid @enderror"
                        value="{{ old('name', ($item != null ? $item->name : '') ) }}">
                    @error('name')
                        <div class="invalid-feedback">
                            {{ $errors->first('name') }}
                        </div>
                    @enderror
                </div>
                <div class="form-group col-12 col-sm-3">
                    <label for="price">Price</label>
                    <input
                        id="price"
                        name="price"
                        type="number"
                        class="form-control @error('price') is-invalid @enderror"
                        min="0.01"
                        max="99999999.99"
                        step="0.01"
                        value="{{ old('price', ($item != null ? $item->price : '') ) }}">
                    @error('price')
                        <div class="invalid-feedback">
                            {{ $errors->first('price') }}
                        </div>
                    @enderror
                </div>
            </div>
            <label>Categories</label>
            <div class="form-group">
                @forelse ($categories as $category)
                    <div class="form-check form-check-inline">
                        <input
                            class="form-check-input"
                            type="checkbox"
                            id="{{ 'categoryCheck'.$category->id }}"
                            value="{{ $category->id }}"
                            name="categories[]"
                            @if ( in_array(
                                    $category->id,
                                    old(
                                        'categories',
                                        ($item != null ? $item->categories->pluck('id')->all() : array())
                                    )
                                )
                            )
                                checked
                            @endif
                            >
                        <label class="form-check-label" for="{{ 'categoryCheck'.$category->id }}">{{ $category->name }}</label>
                    </div>
                @empty
                    <p>There are no categories in the database yet.</p>
                @endforelse
            </div>
            <div class="form-group">
                <label for="description">Description</label>
                <textarea
                    type="textarea"
                    class="form-control @error('description') is-invalid @enderror"
                    id="description"
                    name="description"
                    rows=3>{{
                    old('description', ( $item != null ? $item->description : ''))
                }}</textarea>
                @error('description')
                    <div class="invalid-feedback">
                        {{ $errors->first('description') }}
                    </div>
                @enderror
            </div>
            @if ($item != null and $item->image_url != null)
                <img class="img-thumbnail" src="{{ Storage::url('images/items/' . $item->image_url) }}">
            @endif
            <div class="form-group">
                <div class="custom-file">
                    <label class="custom-file-label" for="image">@if ($item != null and $item->image_url != null) Replace @else Item @endif image</label>
                    <input type="file" class="custom-file-input" id="image" name="image" aria-describedby="imageHelpText">
                    <small id="imageHelpText" class="text-muted">
                        The image can be .png, .jpg, .jpeg, .gif, or .svg, and must not be larger than 4096 KiB.
                    </small>
                    @error('image')
                        <div class="invalid-feedback">
                            {{ $errors->first('image') }}
                        </div>
                    @enderror
                </div>
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
            <a
                role="button"
                type="cancel"
                href="{{ route('manage.items') }}"
                class="btn btn-secondary ml-2">
                Cancel
            </a>
        </form>
    </div>
</div>
@endsection
