@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 px-5">
                <h1> Wellcome!</h1>
                <p>This site has been created as an assignment for the Serverside Webprogramming course at <a href="https://www.elte.hu/en/" target="blank" >Eötvös Lóránd University, Budapest.</a></p>
                <p><b>Developed by:</b> Perlaki András</p>
                <p><b>Neptun ID:</b> CBSDZH</p>
                <p><b>Email:</b> cbsdzh@inf.elte.hu</p>
            </div>
        </div>
    </div>
@endsection
