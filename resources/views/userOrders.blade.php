@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-12">
            <h1>Your Orders</h1>
            @if ($orders == [] || $orders == null)
                <p class="h4">Looks like you haven't ordered anything yet.</p>
            @else
                <ul class="list-group">
                    @foreach ($orders as $order)
                        <li class="list-group-item">
                            <h2>Order id: {{ $order['order_data']->id }}</h2>
                            <h4 class="border-bottom">
                                Total price: &#36; {{ number_format($order['total'], 2) }}
                            </h4>
                            <table class="table table-sm table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col" style="width: 40%">Address</th>
                                        <th scope="col" style="width: 15%">Payment metod</th>
                                        <th scope="col" style="width: 15%">Submitted on</th>
                                        <th scope="col" style="width: 15%">Confirmed on</th>
                                        <th scope="col" style="width: 15%">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td>{{ $order['order_data']->address }}</td>
                                        <td class="text-capitalize">{{ strtolower($order['order_data']->payment_method) }}</td>
                                        <td>{{ $order['order_data']->received_on }}</td>
                                        <td>{{ $order['order_data']->processed_on }}</td>
                                        <td>{{ $order['order_data']->status }}</td>
                                    </tr>
                                </tbody>
                            </table>
                            <h5 class="mt-3">Items:</h5>
                            <ul class="list-group">
                                @foreach ($order["items"] as $item)
                                    <li class="list-group-item  bg-light @if($item["is_deleted"]) text-danger @endif">{{ $item['name'] }}, quantity: {{ $item['quantity'] }}  @if($item["is_deleted"]) ----- Warning! This item is not available right now. @endif</li>
                                @endforeach
                            </ul>
                            <h5 class="mt-3">Comments:</h5>
                            <p class="border rounded px-2 py-1" style="min-height: 3rem">
                                {{ $order['order_data']->comment }}
                            </p>
                        </li>
                    @endforeach
                </ul>
            @endif
        </div>
    </div>
</div>
@endsection
