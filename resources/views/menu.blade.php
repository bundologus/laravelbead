@extends('layouts.app')

@section('content')
    <div class="container">
        <h1>Menu</h1>
        <div class="row mb-4">
            <p class="col-12 h3">
                <span class="mb-2">Categories:</span>
                @foreach ($categories as $category)
                    @if($selected == $category['id'])
                        <a
                            class="badge badge-pill badge-primary mb-1"
                            href="{{ route('menu') }}" >
                            {{ $category["name"] }}
                        </a>
                    @else
                        <a
                            class="badge badge-pill badge-secondary mb-1"
                            href="{{ route('sorted.menu', ['selected' => $category['id']]) }}" >
                            {{ $category["name"] }}
                        </a>
                    @endif
                @endforeach
            </p>
        </div>
        {{--
        |######################################
        | Listing Items by category.
        |###################################### --}}
        @foreach ($categoryItems as $category)
            @if($selected == null || $category['id'] == $selected)
                <div class="row">
                    <h2 class="col-12 border-bottom mt-3">{{ $category['name'] }}</h2>
                    @forelse ($category['items'] as $item)
                        <div class="col-12 col-sm-6 col-md-4">
                            <div class="card mb-4">
                                <img class="card-img-top"
                                    src="@if ($item->image_url != null)
                                        {{ Storage::url('images/items/' . $item->image_url) }}
                                    @else
                                        {{ Storage::url('images/placeholder-image.png')}}
                                    @endif"
                                    alt="item-image">
                                <div class="card-body">
                                    <div class="h4 card-title mb-1">{{ $item->name }}</div>
                                    <p class="h5 card-subtitle mb-2">
                                        @foreach ($item->categories->sortBy('name',SORT_ASC) as $category)
                                            <span class="badge badge-pill badge-secondary mb-1">{{ $category["name"] }}</span>
                                        @endforeach
                                    </p>
                                    <p class="scrollable">{{ $item->description }}</p>
                                    <form action="{{ route('add.to.cart') }}" method="post" class="form-inline justify-content-between">
                                        @csrf
                                        <input
                                            type="hidden"
                                            readonly
                                            class="form-control-plaintext @error('item_id') is-invalid  @enderror"
                                            name="item_id"
                                            value="{{$item->id}}"
                                            >
                                        <input
                                            class="form-control @error('quantity') is-invalid  @enderror"
                                            type="number"
                                            id="quantity"
                                            name="quantity"
                                            value="1"
                                            min="1"
                                            max="10"
                                            @guest disabled @endguest>
                                        <button class="btn btn-primary" type="submit" @guest disabled @endguest>To Cart</button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    @empty
                        <p class="col-12">There are no items in this category.</p>
                    @endforelse
                </div>
            @endif
        @endforeach

        {{--
        |######################################
        | Listing Items with category badges.
        |######################################
        <div class="row">
            @foreach ($items as $item)
                <div class="col-12 col-sm-6 col-md-4">
                    <div class="card mb-4">
                        <img class="card-img-top" src="{{ Storage::url('images/items/' . $item->image_url) }}" alt="item-image">
                        <div class="card-body">
                            <div class="h4 card-title mb-1">{{ $item->name }}</div>
                            <p class="h5 card-subtitle mb-2">
                                @foreach ($item->categories as $category)
                                    <span class="badge badge-pill badge-secondary mb-1">{{ $category["name"] }}</span>
                                @endforeach
                            </p>
                            <p class="scrollable">{{ $item->description }}</p>
                            <form action="{{ route('add.to.cart') }}" method="post" class="form-inline justify-content-between">
                                @csrf
                                <input
                                    type="text"
                                    readonly
                                    class="form-control-plaintext @error('item_id') is-invalid  @enderror"
                                    name="item_id"
                                    value="{{$item->id}}"
                                    >
                                <input
                                    class="form-control @error('quantity') is-invalid  @enderror"
                                    type="number"
                                    id="quantity"
                                    name="quantity"
                                    value="1"
                                    min="1"
                                    max="10"
                                    @guest disabled @endguest>
                                <button class="btn btn-primary" type="submit" @guest disabled @endguest>To Cart</button>
                            </form>
                        </div>
                    </div>
                </div>
            @endforeach
        </div> --}}
    </div>
@endsection
