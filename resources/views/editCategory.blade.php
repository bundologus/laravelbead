@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <h1>@if ($id == '') New category @else Editing category @endif</h1>
        <form
            class="col-12"
            method="POST"
            @if($id == '')
                action="{{ route('store.category') }}"
            @else
                action="{{ route('update.category', ['id' => $id]) }}"
            @endif
            >
            @csrf
            <input type="hidden" id="id" name="id" value="{{$id}}" readonly>
            <div class="form-group">
                <label for="name">Category name</label>
                <input
                    id="name"
                    name="name"
                    type="text"
                    class="form-control @error('name')is-invalid @enderror"
                    value="{{ old('name', $name) }}">
                @error('name')
                    <div class="invalid-feedback">
                        {{ $errors->first('name') }}
                    </div>
                @enderror
            </div>
            <button type="submit" class="btn btn-primary">Submit</button>
            <a
                role="button"
                type="cancel"
                href="{{ route('manage.categories') }}"
                class="btn btn-secondary ml-2">
                Cancel
            </a>
        </form>
    </div>
</div>
@endsection
