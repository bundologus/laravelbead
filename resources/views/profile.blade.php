@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-6">
            <h1>User Profile</h1>
            <table class="table">
            <tbody>
                <tr>
                    <th scope="row">Name</th>
                    <td>{{ $user->name }}</td>
                </tr>
                <tr>
                    <th scope="row">Email</th>
                    <td>{{ $user->email }}</td>
                </tr>
                <tr>
                    <th scope="row">Access level</th>
                    <td>@if ($user->is_admin) admin @else regular user @endif</td>
                </tr>
            </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
