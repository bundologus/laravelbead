@extends('layouts.app')

@php
    $received = $received ?? null;
    $accepted = $accepted ?? null;
    $rejected = $rejected ?? null;
@endphp

@section('content')
<div class="container">
    <div class="row">
        <h1 class="col-12">Manage orders</h1>
        <div class="col-12 col-md-6">
            <a
                role="button"
                class="btn btn-primary btn-lg w-100 @if ($received != null) disabled @endif"
                href="{{ route('manage.received') }}"
                @if ($received != null) aria-disabled="true" @endif>
                View received orders
            </a>
        </div>
        <div class="col-12 col-md-6">
            <a
                role="button"
                class="btn btn-secondary btn-lg w-100 @if ($accepted != null or $rejected != null) disabled @endif"
                href="{{ route('manage.processed') }}"
                @if ($received == null) aria-disabled="true" @endif>
                View processed orders
            </a>
        </div>
        @if ($received != null)
            <div class="col-12 mt-3">
                <h2>Received orders</h2>
                @if(count($received) == 0)
                    <p>There are no pending orders.</p>
                @endif
                <div class="accordion" id="receivedAccordion">
                    @foreach ($received as $order)
                        <div class="card">
                            <div class="card-header" id="heading{{$order['order_data']->id}}">
                                <button
                                    class="btn btn-link btn-block text-left"
                                    type="button"
                                    data-toggle="collapse"
                                    data-target="#collapse{{$order['order_data']->id}}"
                                    aria-expanded="false"
                                    aria-controls="collapse{{$order['order_data']->id}}">
                                    <h2>Order id: {{ $order['order_data']->id }}</h2>
                                    <h4>Total price: &#36; {{ number_format($order['total'], 2) }}</h4>
                                </button>
                            </div>
                            <div
                                id="collapse{{$order['order_data']->id}}"
                                class="collapse"
                                aria-labelledby="heading{{$order['order_data']->id}}"
                                data-parent="#receivedAccordion">
                                <div class="card-body">
                                    <h5>Buyer</h5>
                                    <table class="table table-sm table-bordered">
                                        <thead>
                                            <tr>
                                                <th scope="col" style="width: 40%">Name</th>
                                                <th scope="col" style="width: 30%">Email</th>
                                                <th scope="col" style="width: 30%">Registration date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{ $order['order_data']->user->name }}</td>
                                                <td>{{ $order['order_data']->user->email }}</td>
                                                <td>{{ $order['order_data']->user->created_at }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <h5>Order data</h5>
                                    <table class="table table-sm table-bordered">
                                        <thead>
                                            <tr>
                                                <th scope="col" style="width: 40%">Address</th>
                                                <th scope="col" style="width: 15%">Payment metod</th>
                                                <th scope="col" style="width: 15%">Submitted on</th>
                                                <th scope="col" style="width: 15%">Confirmed on</th>
                                                <th scope="col" style="width: 15%">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{ $order['order_data']->address }}</td>
                                                <td class="text-capitalize">{{ strtolower($order['order_data']->payment_method) }}</td>
                                                <td>{{ $order['order_data']->received_on }}</td>
                                                <td>{{ $order['order_data']->processed_on }}</td>
                                                <td>{{ $order['order_data']->status }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <h5 class="mt-3">Items:</h5>
                                    <table class="table table-sm table-bordered">
                                        <thead>
                                            <tr>
                                                <th scope="col" style="width: 40%">Name</th>
                                                <th scope="col" style="width: 30%">Quantity</th>
                                                <th scope="col" style="width: 30%">Availability</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($order["items"] as $item)
                                                <tr class="@if($item["is_deleted"]) text-danger @endif">
                                                    <td>{{ $item['name'] }}</td>
                                                    <td>{{ $item['quantity'] }}</td>
                                                    <td>@if($item["is_deleted"]) Warning! This item is not available right now. @else Available. @endif</td>
                                                <tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <h5 class="mt-3">Comments:</h5>
                                    <p class="border rounded px-2 py-1" style="min-height: 3rem">
                                        {{ $order['order_data']->comment }}
                                    </p>
                                    <h5>Actions</h5>
                                    <div class="row">
                                        <form class="col-12 col-sm-6" method="POST" action="{{ route('accept.order', ['id'=> $order['order_data']->id])}}">
                                            @csrf
                                            <input type="hidden" id="id" name="id" value="{{ $order['order_data']->id }}" readonly>
                                            <button type="submit" class="btn btn-success btn-block">Accept order</button>
                                        </form>
                                        <form class="col-12 col-sm-6" method="POST" action="{{ route('reject.order', ['id'=> $order['order_data']->id])}}">
                                            @csrf
                                            <input type="hidden" id="id" name="id" value="{{ $order['order_data']->id }}" readonly>
                                            <button type="submit" class="btn btn-danger btn-block">Reject order</button>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @endif
        @if ($accepted !=null)
            <div class="col-12 mt-3">
                <h2>Accepted orders</h2>
                @if(count($accepted) == 0)
                    <p>There are no accepted orders.</p>
                @endif
                <div class="accordion" id="acceptedAccordion">
                    @foreach ($accepted as $order)
                        <div class="card">
                            <div class="card-header" id="heading{{$order['order_data']->id}}">
                                <button
                                    class="btn btn-link btn-block text-left"
                                    type="button"
                                    data-toggle="collapse"
                                    data-target="#collapse{{$order['order_data']->id}}"
                                    aria-expanded="false"
                                    aria-controls="collapse{{$order['order_data']->id}}">
                                    <h2>Order id: {{ $order['order_data']->id }}</h2>
                                    <h4>Total price: &#36; {{ number_format($order['total'], 2) }}</h4>
                                </button>
                            </div>
                            <div
                                id="collapse{{$order['order_data']->id}}"
                                class="collapse"
                                aria-labelledby="heading{{$order['order_data']->id}}"
                                data-parent="#acceptedAccordion">
                                <div class="card-body">
                                    <h5>Buyer</h5>
                                    <table class="table table-sm table-bordered">
                                        <thead>
                                            <tr>
                                                <th scope="col" style="width: 40%">Name</th>
                                                <th scope="col" style="width: 30%">Email</th>
                                                <th scope="col" style="width: 30%">Registration date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{ $order['order_data']->user->name }}</td>
                                                <td>{{ $order['order_data']->user->email }}</td>
                                                <td>{{ $order['order_data']->user->created_at }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <h5>Order data</h5>
                                    <table class="table table-sm table-bordered">
                                        <thead>
                                            <tr>
                                                <th scope="col" style="width: 40%">Address</th>
                                                <th scope="col" style="width: 15%">Payment metod</th>
                                                <th scope="col" style="width: 15%">Submitted on</th>
                                                <th scope="col" style="width: 15%">Confirmed on</th>
                                                <th scope="col" style="width: 15%">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{ $order['order_data']->address }}</td>
                                                <td class="text-capitalize">{{ strtolower($order['order_data']->payment_method) }}</td>
                                                <td>{{ $order['order_data']->received_on }}</td>
                                                <td>{{ $order['order_data']->processed_on }}</td>
                                                <td>{{ $order['order_data']->status }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <h5 class="mt-3">Items:</h5>
                                    <table class="table table-sm table-bordered">
                                        <thead>
                                            <tr>
                                                <th scope="col" style="width: 40%">Name</th>
                                                <th scope="col" style="width: 30%">Quantity</th>
                                                <th scope="col" style="width: 30%">Availability</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($order["items"] as $item)
                                                <tr class="@if($item["is_deleted"]) text-danger @endif">
                                                    <td>{{ $item['name'] }}</td>
                                                    <td>{{ $item['quantity'] }}</td>
                                                    <td>@if($item["is_deleted"]) Warning! This item is not available right now. @else Available. @endif</td>
                                                <tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <h5 class="mt-3">Comments:</h5>
                                    <p class="border rounded px-2 py-1" style="min-height: 3rem">
                                        {{ $order['order_data']->comment }}
                                    </p>
                                    <h5>Actions</h5>
                                    <p>Nothing to do, this order has already been processed.</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @endif
        @if ($rejected !=null)
            <div class="col-12 mt-4">
                <h2>Rejected orders</h2>
                @if(count($rejected) == 0)
                    <p>There are no rejected orders.</p>
                @endif
                <div class="accordion" id="rejectedAccordion">
                    @foreach ($rejected as $order)
                        <div class="card">
                            <div class="card-header" id="heading{{$order['order_data']->id}}">
                                <button
                                    class="btn btn-link btn-block text-left"
                                    type="button"
                                    data-toggle="collapse"
                                    data-target="#collapse{{$order['order_data']->id}}"
                                    aria-expanded="false"
                                    aria-controls="collapse{{$order['order_data']->id}}">
                                    <h2>Order id: {{ $order['order_data']->id }}</h2>
                                    <h4>Total price: &#36; {{ number_format($order['total'], 2) }}</h4>
                                </button>
                            </div>
                            <div
                                id="collapse{{$order['order_data']->id}}"
                                class="collapse"
                                aria-labelledby="heading{{$order['order_data']->id}}"
                                data-parent="#rejectedAccordion">
                                <div class="card-body">
                                    <h5>Buyer</h5>
                                    <table class="table table-sm table-bordered">
                                        <thead>
                                            <tr>
                                                <th scope="col" style="width: 40%">Name</th>
                                                <th scope="col" style="width: 30%">Email</th>
                                                <th scope="col" style="width: 30%">Registration date</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{ $order['order_data']->user->name }}</td>
                                                <td>{{ $order['order_data']->user->email }}</td>
                                                <td>{{ $order['order_data']->user->created_at }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <h5>Order data</h5>
                                    <table class="table table-sm table-bordered">
                                        <thead>
                                            <tr>
                                                <th scope="col" style="width: 40%">Address</th>
                                                <th scope="col" style="width: 15%">Payment metod</th>
                                                <th scope="col" style="width: 15%">Submitted on</th>
                                                <th scope="col" style="width: 15%">Confirmed on</th>
                                                <th scope="col" style="width: 15%">Status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>{{ $order['order_data']->address }}</td>
                                                <td class="text-capitalize">{{ strtolower($order['order_data']->payment_method) }}</td>
                                                <td>{{ $order['order_data']->received_on }}</td>
                                                <td>{{ $order['order_data']->processed_on }}</td>
                                                <td>{{ $order['order_data']->status }}</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <h5 class="mt-3">Items:</h5>
                                    <table class="table table-sm table-bordered">
                                        <thead>
                                            <tr>
                                                <th scope="col" style="width: 40%">Name</th>
                                                <th scope="col" style="width: 30%">Quantity</th>
                                                <th scope="col" style="width: 30%">Availability</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($order["items"] as $item)
                                                <tr class="@if($item["is_deleted"]) text-danger @endif">
                                                    <td>{{ $item['name'] }}</td>
                                                    <td>{{ $item['quantity'] }}</td>
                                                    <td>@if($item["is_deleted"]) Warning! This item is not available right now. @else Available. @endif</td>
                                                <tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                    <h5 class="mt-3">Comments:</h5>
                                    <p class="border rounded px-2 py-1" style="min-height: 3rem">
                                        {{ $order['order_data']->comment }}
                                    </p>
                                    <h5>Actions</h5>
                                    <p>Nothing to do, this order has already been processed.</p>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        @endif
    </div>
</div>
@endsection
