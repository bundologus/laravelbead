@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <h1>Manage items</h1>
        <div class="col-12">
            @foreach ($items as $item)
                <div class="border  @if($item->deleted_at != null) bg-light border-secondary @else border-dark @endif rounded row mb-1">
                    <div class="media @if($item->deleted_at != null) text-black-50 @endif my-2 col-11">
                        <img class="mr-3 img-thumbnail" style="max-height: 6rem" src="@if ($item->image_url != null)
                                {{ Storage::url('images/items/' . $item->image_url) }}
                            @else
                                {{ Storage::url('images/placeholder-image.png')}}
                            @endif"
                            alt="item-image">
                        <div class="media-body">
                            <p class="h5 my-0">{{ $item->name }} @if($item->deleted_at != null) <span class="text-danger">deleted at: {{ $item->deleted_at }} </span> @endif</p>
                            <p class="text-muted mb-2">
                                @forelse ($item->categories->sortBy('name',SORT_ASC) as $category)
                                    {{ $category->name.", " }}
                                @empty
                                    The item has not been added to any categories.
                                @endforelse
                            </p>
                            {{ $item->description }}
                        </div>
                    </div>
                    <div class="d-flex flex-column justify-content-around p-3 col-1">
                        <a role="button" href="{{ route('edit.item', ['id' => $item->id]) }}" class="btn btn-block btn-outline-primary">
                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-pencil" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                <path fill-rule="evenodd" d="M12.146.146a.5.5 0 0 1 .708 0l3 3a.5.5 0 0 1 0 .708l-10 10a.5.5 0 0 1-.168.11l-5 2a.5.5 0 0 1-.65-.65l2-5a.5.5 0 0 1 .11-.168l10-10zM11.207 2.5L13.5 4.793 14.793 3.5 12.5 1.207 11.207 2.5zm1.586 3L10.5 3.207 4 9.707V10h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.293l6.5-6.5zm-9.761 5.175l-.106.106-1.528 3.821 3.821-1.528.106-.106A.5.5 0 0 1 5 12.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.468-.325z"/>
                            </svg>
                        </a>
                        @if ($item->deleted_at != null)
                            <form method='POST' action="{{ route('restore.item', ['id' => $item->id]) }}">
                                @csrf
                                <input type="hidden" id="id" name="id" value="{{ $item->id }}" readonly>
                                <button type="submit" class="btn btn-block btn-success mt-2">
                                    <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-bootstrap-reboot" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M1.161 8a6.84 6.84 0 1 0 6.842-6.84.58.58 0 0 1 0-1.16 8 8 0 1 1-6.556 3.412l-.663-.577a.58.58 0 0 1 .227-.997l2.52-.69a.58.58 0 0 1 .728.633l-.332 2.592a.58.58 0 0 1-.956.364l-.643-.56A6.812 6.812 0 0 0 1.16 8zm5.48-.079V5.277h1.57c.881 0 1.416.499 1.416 1.32 0 .84-.504 1.324-1.386 1.324h-1.6zm0 3.75V8.843h1.57l1.498 2.828h1.314L9.377 8.665c.897-.3 1.427-1.106 1.427-2.1 0-1.37-.943-2.246-2.456-2.246H5.5v7.352h1.141z"/>
                                    </svg>
                                </button>
                            </form>
                        @else
                            <a role="button" href="{{ route('confirm.delete', ['type' => 'item', 'id' => $item->id]) }}" class="btn btn-block btn-danger">
                                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-trash" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"/>
                                    <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"/>
                                </svg>
                            </a>
                        @endif
                    </div>
                </div>
            @endforeach
            <a role=button href="{{ route('new.item') }}" class="btn btn-success mr-2 mt-2">
                <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-plus" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M8 4a.5.5 0 0 1 .5.5v3h3a.5.5 0 0 1 0 1h-3v3a.5.5 0 0 1-1 0v-3h-3a.5.5 0 0 1 0-1h3v-3A.5.5 0 0 1 8 4z"/>
                </svg>
                Add new item
            </a>
        </div>
    </div>
</div>
@endsection
