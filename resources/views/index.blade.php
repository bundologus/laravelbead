@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-12 d-flex justify-content-center">
                <div class="card p-3 vertical-center" style="width: fit-content">
                    <div class='card-body'>
                        <h1> Wellcome!</h1>
                        <p class="h4">The site has:</p>
                        <div class="list-group">
                            <div class="list-group-item">
                                Users <span class="badge badge-primary float-right">{{ $users }}</span>
                            </div>
                            <div class="list-group-item">
                                Categories <span class="badge badge-primary float-right">{{ $categories }}</span>
                            </div>
                            <div class="list-group-item">
                                Items <span class="badge badge-primary float-right">{{ $items }}</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
