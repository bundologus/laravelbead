@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <h1>Cart</h1>
    </div>
    @guest
        <div class="alert alert-dark alert-dismissible fade show" role="alert">
            You must be logged in to view this page.
        </div>
    @else
        @error('item_id')
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('item_id') }}
            </div>
        @enderror
        @error('quantity')
            <div class="alert alert-danger" role="alert">
                {{ $errors->first('quantity') }}
            </div>
        @enderror
        @if (session()->has('item_removed'))
            <div class="alert alert-success" role="alert">
                {{ session()->get('item_removed') }} removed from the cart.
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
        @endif
        @if ($items == null)
            @error('order_id')
                <div class="alert alert-danger">
                    {{ $errors->first('order_id') }}
                </div>
            @enderror
            <div class="row">
                <h2>Your cart is empty :(</h2>
            </div>
        @else
            <div class="row">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col" style="width: 5%"></th>
                            <th scope="col">Item</th>
                            <th scope="col">quantity</th>
                            <th scope="col">unit price</th>
                            <th scope="col">subtotal</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($items as $item)
                            <tr>
                                <td>
                                    <form method="POST" action="{{ route('remove.from.cart', ['id' => $item['orderedItem_id']]) }}">
                                        @csrf
                                        <button type="submit"  class="btn btn-outline-danger">
                                            <svg width="1em" height="1em" viewBox="0 0 16 16" class="bi bi-trash" fill="currentColor" xmlns="http://www.w3.org/2000/svg">
                                                <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"></path>
                                                <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"></path>
                                            </svg>
                                        </button>
                                    </form>
                                </td>
                                <th scope="row">{{ $item['itemData']->name }}</th>
                                <td>{{ $item['quantity'] }}</td>
                                <td>{{ $item['itemData']->price }}</td>
                                <td>{{ $item['itemData']->price * $item['quantity'] }}</td>
                            </tr>
                        @endforeach
                        <tr class="table-dark">
                            <td></td>
                            <th class="table-dark" scope="row" colspan=3>Total:</th>
                            <td class="table-dark" >{{ $total }}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
            <div class="row">
                <form class="col-12" method="POST" action="{{ route('send.order') }}">
                    @csrf
                    <input
                        type="hidden"
                        readonly
                        class="form-control-plaintext"
                        name="order_id"
                        value="{{$order_id}}"
                        >
                    @error('order_id')
                        <p class="text-danger">
                            {{ $errors->first('order_id') }}
                        </p>
                    @enderror
                    <p>The fields marked with a * are mandatory.</p>
                    <div class="form-group">
                        <label for="address">*Delivery Address</label>
                        <input
                            type="text"
                            class="form-control @error('address') is-invalid @enderror"
                            id="address"
                            name="address"
                            value="{{ (old('address') == null) ? $address : old('address') }}">
                        @error('address')
                            <div class="invalid-feedback">
                                {{ $errors->first('address') }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="comment">Comments</label>
                        <textarea type="textarea" class="form-control" id="comment" name="comment" rows=3>{{ old('comment') }}</textarea>
                    </div>
                    <div class="form-group">
                        <label>*Payment Method:</label>
                        @foreach($payment_methods as $method)
                            <div class="custom-control custom-radio custom-control-inline ml-5">
                                <input
                                    class="custom-control-input @error('payment_method') is-invalid @enderror"
                                    type="radio"
                                    name="payment_method"
                                    id="{{ $method }}"
                                    value="{{ $method }}">
                                <label class="custom-control-label" for="{{ $method }}">
                                    <span class="text-capitalize">{{ strtolower($method) }}
                                </label>
                            </div>
                        @endforeach
                        @error('payment_method')
                            <div class="invalid-feedback">
                                {{ $errors->first('payment_method') }}
                            </div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary" @cannot ('update-cart', $order_id) disabled @endcannot>Submit Your Order</button>
                </form>
            </div>
        @endif
    @endguest
</div>
@endsection
