<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'address',
        'comment',
        'payment_method',
        'status',
        'received_on',
        'processed_on'
    ];

    public function user() {
        return $this->belongsTo(User::class);
    }

    public function orderedItems() {
        return $this->hasMany(OrderedItem::class);
    }

    public function totalPrice() {
        $total = 0;
        $items = $this->orderedItems->map(function ($orderedItem) {
            return [
                'price' => Item::withTrashed()->find($orderedItem->item_id)->price,
                'quantity' => $orderedItem->quantity
            ];
        });
        foreach($items as $item) {
            $total += $item['price'] * $item['quantity'];
        }
        return $total;
    }
}
