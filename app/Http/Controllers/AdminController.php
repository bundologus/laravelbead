<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\SubmitCategoryRequest;
use App\Http\Requests\SubmitItemRequest;
use App\Http\Requests\UpdateOrderRequest;
use App\Models\Category;
use App\Models\Item;
use App\Models\Order;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class AdminController extends Controller
{
    public function adminCategories() {
        $categories = Category::get()->sortBy('name', SORT_ASC)->all();
        return view('adminCategories', compact('categories'));
    }

    public function newCategory() {
        return view('editCategory', ['name' => '', 'id' => '']);
    }

    public function storeNewCategory(SubmitCategoryRequest $request) {
        $data = $request->all();

        DB::table('categories')->insert(['name' => $data['name']]);

        $alert = 'Category '.$data['name'].' has been created.';

        return redirect()->route('manage.categories')->with('alert-success', $alert);
    }

    public function editCategory($id) {
        $category = Category::find($id);
        return view('editCategory', ['name' => $category->name, 'id' => $category->id]);
    }

    public function updateCategory(SubmitCategoryRequest $request) {
        $data = $request->all();
        $result = 'alert-danger';
        $alert = 'Something went wrong. The caregory was not updated.';


        if($data['id'] != null) {
            $cat = Category::find($data['id']);
            $oldname = $cat->name;
            $cat->name = $data['name'];
            $cat->save();
            $result = 'alert-success';
            $alert = 'The category '.$oldname.' has been renamed to '.$data['name'].".";
        }

        return redirect()->route('manage.categories')->with($result, $alert);
    }

    public function deleteCategory(Request $request) {
        $data = $request->validate([
            'id' => 'required|numeric|exists:categories,id'
        ]);

        $cat = Category::find($data['id']);
        $name = $cat->name;
        $alert = 'Category '.$name.' has been deleted.';
        $cat->delete();

        return redirect()->route('manage.categories')->with('alert-success', $alert);
    }

    public function adminItems() {
        $items = Item::withTrashed()->get()->sortBy('name')->all();
        return view('adminItems', compact('items'));
    }

    public function newItem() {
        $item = null;
        $categories = Category::get()->sortBy('name', SORT_ASC)->all();
        return view('editItem', compact('item', 'categories'));
    }

    public function storeNewItem(SubmitItemRequest $request) {
        $data = $request->all();
        $alert_type = 'alert-danger';
        $alert = 'Something went wrong. The item was not created.';
        $hashName = '';

        if ($request->hasFile('image')) {
            $file = $request->file('image');
            $hashName = $file->hashName();
            Storage::disk('public')->put('images/items/'.$hashName, file_get_contents($file));
        }

        try {
            $item = Item::create([
                'name' => $data['name'],
                'description' => $data['description'],
                'price' => $data['price'],
                'image_url' => $hashName,
            ]);

            $item->categories()->attach($data['categories']);

            $alert_type = 'alert-success';
            $alert = 'The item '.$data['name'].' has been created with price '.$data['price'].'.';
        } catch (\Throwable $th) {
            //throw $th;

            $alert_type = 'alert-warning';
            $alert = 'A database error happened, the item '.$data['name'].' could not be created.';
        }

        return redirect()->route('manage.items')->with($alert_type, $alert);
    }

    public function editItem($id) {
        $item = Item::withTrashed()->find($id);
        $categories = Category::get()->sortBy('name', SORT_ASC)->all();
        return view('editItem', compact('item', 'categories'));
    }

    public function updateItem(SubmitCategoryRequest $request) {
        $data = $request->all();
        $alert_type = 'alert-danger';
        $alert = 'Something went wrong. The item was not updated.';

        if($data['id'] != null) {
            $item = Item::find($data['id']);
            $alert_type = 'alert-success';
            $alert = 'Nothing was changed on the item '.$item->name.'.';
            $changes = array();

            $oldName = $item->name;
            if ($oldName != $data['name']) {
                $item->name = $data['name'];
                array_push($changes, 'name: '.$oldName.' -> '.$data['name']);
            }

            $oldDesc = $item->description;
            if ($oldDesc != $data['description']) {
                $item->description = $data['description'];
                array_push($changes, 'description (see below)');
            }

            $oldPrice = $item->price;
            if ($oldPrice != $data['price']) {
                $item->price = $data['price'];
                array_push($changes, 'price: '.$oldPrice.' -> '.$data['price']);
            }

            $oldCategories = $item->categories->pluck('id')->all();
            $categoryAdded = array_diff($data['categories'], $oldCategories);
            $categoryRemoved = array_diff($oldCategories, $data['categories']);
            $item->categories()->attach($categoryAdded);
            $item->categories()->detach($categoryRemoved);

            $removedCateg = "";
            $addedCateg = "";
            foreach ($categoryRemoved as $key => $value) {
                $removedCateg = $removedCateg.Category::find($value)->name.', ';
            }
            foreach ($categoryAdded as $key => $value) {
                $addedCateg = $addedCateg.Category::find($value)->name.', ';
            }
            if ($removedCateg != "") {
                array_push($changes, "categories removed: ".$removedCateg);
            }
            if ($addedCateg != "") {
                array_push($changes, "categories added: ".$addedCateg);
            }


            if ($request->hasFile('image')) {
                $file = $request->file('image');
                $hashName = $file->hashName();
                Storage::disk('public')->put('images/items/'.$hashName, file_get_contents($file));
                $item->image_url = $hashName;

                array_push($changes, 'image (see below)');
            }

            if ( count($changes) > 0 ) {
                $item->save();
                $alert = "The following changes were made to the item ".$oldName.":";
                foreach ($changes as $i => $text) {
                    $alert = $alert." | ".$text;
                }
            }
        }

        return redirect()->route('manage.items')->with($alert_type, $alert);
    }

    public function deleteItem(Request $request) {
        $data = $request->validate([
            'id' => 'required|numeric|exists:items,id'
        ]);

        $item = Item::find($data['id']);
        $name = $item->name;
        $alert = 'Item '.$name.' has been deleted.';
        $item->delete();

        return redirect()->route('manage.items')->with('alert-success', $alert);
    }

    public function restoreItem(Request $request) {
        $data = $request->validate([
            'id' => 'required|numeric|exists:items,id'
        ]);

        $alertType = 'alert-danger';
        $alert = 'Something went wrong. The item could not be restored.';

        $item = Item::onlyTrashed()->find($data['id']);
        if($item != null) {
            $name = $item->name;
            $alertType = 'alert-success';
            $alert = 'Item '.$name.' has been restored.';
            $item->restore();
        }

        return redirect()->route('manage.items')->with($alertType, $alert);
    }

    public function adminOrders() {
        return view('adminOrders');
    }

    public function viewReceived() {
        $received = Order::all()->where('status', 'RECEIVED')->map(function ($order) {
            return [
                'order_data' => $order,
                'items' => $order->orderedItems->map(function ($orderedItem) {
                    $item = Item::withTrashed()->find($orderedItem->item_id);
                    return [
                        'name' => $item->name,
                        'quantity' => $orderedItem->quantity,
                        'is_deleted' => ($item->deleted_at != null),
                    ];
                }),
                'total' => $order->totalPrice()
            ];
        });

        return view('adminOrders', compact('received'));
    }

    public function viewProcessed() {
        $accepted = Order::all()->where('status', 'ACCEPTED')->map(function ($order) {
            return [
                'order_data' => $order,
                'items' => $order->orderedItems->map(function ($orderedItem) {
                    $item = Item::withTrashed()->find($orderedItem->item_id);
                    return [
                        'name' => $item->name,
                        'quantity' => $orderedItem->quantity,
                        'is_deleted' => ($item->deleted_at != null),
                    ];
                }),
                'total' => $order->totalPrice()
            ];
        });
        $rejected = Order::all()->where('status', 'REJECTED')->map(function ($order) {
            return [
                'order_data' => $order,
                'items' => $order->orderedItems->map(function ($orderedItem) {
                    $item = Item::withTrashed()->find($orderedItem->item_id);
                    return [
                        'name' => $item->name,
                        'quantity' => $orderedItem->quantity,
                        'is_deleted' => ($item->deleted_at != null),
                    ];
                }),
                'total' => $order->totalPrice()
            ];
        });

        return view('adminOrders', compact('accepted','rejected'));
    }

    public function acceptOrder(UpdateOrderRequest $request) {
        $order = Order::find($request->id);

        $order->status = 'ACCEPTED';
        $order->processed_on = now();

        $order->save();

        return redirect()->route('manage.received')->with('alert-success', 'The order #'.$order->id.' has been accepted.');
    }

    public function rejectOrder(UpdateOrderRequest $request) {
        $order = Order::find($request->id);

        $order->status = 'REJECTED';
        $order->processed_on = Carbon::now();

        $order->save();

        return redirect()->route('manage.received')->with('alert-success', 'The order #'.$order->id.' has been rejected.');
    }

    public function deteleConfirmation($type,$id) {
        if ($type == 'category') {
            $obj = Category::find($id);
            if ($obj == null) {
                return redirect()->route('manage.categories')->with('alert-danger', 'Something went wrong. Could not find category.');
            }
        } else if ($type = 'item') {
            $obj = Item::find($id);
            if ($obj == null) {
                return redirect()->route('manage.items')->with('alert-danger', 'Something went wrong. Could not find item.');
            }
        }

        return view('deleteConfirm', ['type' => $type, 'id' => $id, 'obj' => $obj]);
    }
}
