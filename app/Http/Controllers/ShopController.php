<?php

namespace App\Http\Controllers;

use App\Http\Requests\AddToCartRequest;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Category;
use App\Models\Item;
use App\Models\Order;
use App\Models\OrderedItem;
use App\Models\User;
use Carbon\Carbon;

class ShopController extends Controller
{
    public function showMenu($selected = null) {
        $items = Item::all();
        $categories = Category::get()->sortBy('name', SORT_ASC);
        $categoryItems = $categories->map(function ($category) {
            return [
                "id" => $category->id,
                "name" => $category->name,
                "items" => $category->items->all()
            ];
        });

        return view('menu', compact('items', 'selected', 'categories', 'categoryItems'));
    }

    public function addToCart(AddToCartRequest $request) {
        $data = $request->all();

        $user_id = Auth::id();

        $cart = User::find($user_id)->orders->where('status', 'CART')->first();

        if ($cart == null) {
            $cart = User::find($user_id)->orders()->create(['status' => 'CART']);
        }

        $orderedItem = $cart->orderedItems->where('item_id', $data['item_id'])->first();
        if ($orderedItem == null) {
            $cart->orderedItems()->create(['item_id' => $data['item_id'], 'quantity' => $data['quantity']]);
        } else {
            $orderedItem->quantity = $orderedItem->quantity + $data['quantity'];
            $orderedItem->save();
        }

        /* $message = [
            'status' => 'testing',
            'content' => $errors;
        ]; */

        $alert = Item::find($data['item_id'])->name . " has been added to the cart.";

        return redirect()->route('cart')->with('alert-success', $alert);
    }

    public function showCart(){
        $user = Auth::user();
        $order = $user->orders->where('status', 'CART')->first();
        $payment_methods = config("enums.payment_methods");
        $order_id = null;
        $address = null;
        $items = null;
        $total = 0;

        if ($order != null) {
            $items = $order->orderedItems->map(function ($orderedItem) {
                return [
                    'orderedItem_id' => $orderedItem->id,
                    'quantity' => $orderedItem->quantity,
                    'itemData' => Item::find($orderedItem->item_id),
                ];
            })->all();

            $order_id = $order->id;
            $address = $order->address;

            foreach($items as $item) {
                $total = $total + ($item['quantity'] * $item['itemData']->price);
            }
        }

        return view('cart', compact('items', 'total', 'order_id', 'payment_methods', 'address'));
    }

    public function removeItem($id) {
        $orderedItem = OrderedItem::find($id);
        $itemName = Item::find($orderedItem->item_id)->name;

        $orderedItem->delete();

        $alert = $itemName . ' removed from cart.';

        return redirect()->route('cart')->with('alert-success', $alert);
    }

    public function sendOrder(Request $request) {
        $data = $request->validate([
            'order_id' => [
                'required',
                'integer',
                'exists:orders,id',
                function($attribute, $value, $fail) {
                    $order = Order::find($value);
                    if ($order->orderedItems->all() == []) {
                        $fail('The cart must not be empty.');
                    };
                    if ($order->status != 'CART') {
                        $fail('We encountered an error and your order was not sent.');
                    }
                }
            ],
            'address' => 'required|string|max:255',
            'comment' => 'nullable|string|max:500',
            'payment_method' => 'required|string'
        ]);

        $order = Order::find($data['order_id']);
        $order->address = $data['address'];
        if ($data['comment'] != null){
            $order->comment = $data['comment'];
        }
        $order->payment_method = $data['payment_method'];
        $order->status = 'RECEIVED';
        $order->received_on = Carbon::now();
        $order->save();

        return redirect()->route('menu')->with('alert-success', 'Your order has been sent.');
    }
}
