<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Models\Category;
use App\Models\Item;
use App\Models\Order;
use App\Models\User;

class MainController extends Controller
{
    public function index() {
        $users = User::count();
        $categories = Category::count();
        $items = Item::count();

        return view('index', compact('users', 'categories', 'items'));
    }

    public function about() {
        return view('about');
    }

    public function showProfile() {
        $user = Auth::user();
        return view('profile', compact('user'));
    }

    public function showOrders() {
        $user = Auth::id();
        $orders = Order::all()->where('user_id', $user)->where('status', '<>', 'CART')->map(function ($order) {
            // somehow, 'where([ ['user_id', '=', $user], ['status', '<>', 'CART'] ])' did not work
            return [
                'order_data' => $order,
                'items' => $order->orderedItems->map(function ($orderedItem) {
                    $item = Item::withTrashed()->find($orderedItem->item_id);
                    return [
                        'name' => $item->name,
                        'quantity' => $orderedItem->quantity,
                        'is_deleted' => ($item->deleted_at != null),
                    ];
                }),
                'total' => $order->totalPrice()
            ];
        });
        return view('userOrders', compact('orders'));
    }
}
