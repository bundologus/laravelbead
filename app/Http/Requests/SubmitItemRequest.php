<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class SubmitItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->is_admin;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'id' => 'nullable|integer|exists:items,id',
            'name' => 'required|min:3|max:255|unique:items,name',
            'description' => 'required|min:7|max:4096',
            'price' => 'required|numeric|min:0.01|max:99999999.99|regex:/^\d{1,8}(?:\.\d{1,2})?$/',
            'image' => 'nullable|image|mimes:png,jpg,jpeg,gif,svg|max:4096'
        ];
    }

    public function messages()
    {
        return [
            'price.regex' => 'The price must contain 1-to-8 digits and optionally a decimal point followed by 1 or 2 decimal digits.'
        ];
    }
}
