<?php

use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\MainController;
use App\Http\Controllers\ShopController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('welcome');
});*/

Auth::routes();

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('/', [MainController::class, 'index'])->name('landing');
Route::get('/about', [MainController::class, 'about'])->name('about');
Route::get('/menu', [ShopController::class, 'showMenu'])->name('menu');
Route::get('/menu/category/{selected}', [ShopController::class, 'showMenu'])->name('sorted.menu');
Route::Post('/cart/add', [ShopController::class, 'addToCart'])->name('add.to.cart')->middleware('auth');
Route::Post('/cart/remove/{id}', [ShopController::class, 'removeItem'])->name('remove.from.cart')->middleware('auth');
Route::Post('/cart/send', [ShopController::class, 'sendOrder'])->name('send.order')->middleware('auth');
Route::get('/cart', [ShopController::class, 'showCart'])->name('cart')->middleware('auth');
Route::get('/profile', [MainController::class, 'showProfile'])->name('profile')->middleware('auth');
Route::get('/orders', [MainController::class, 'showOrders'])->name('user.orders')->middleware('auth');

// admin
Route::get('/admin/category', [AdminController::class, 'adminCategories'])->name('manage.categories')->middleware('can:viewAny,App\Model\Category');
Route::get('/admin/category/new', [AdminController::class, 'newCategory'])->name('new.category')->middleware('can:create,App\Model\Category');
Route::post('/admin/category/store', [AdminController::class, 'storeNewCategory'])->name('store.category')->middleware('can:create,App\Model\Category');
Route::get('/admin/category/{id}/edit', [AdminController::class, 'editCategory'])->name('edit.category')->middleware('can:update,App\Model\Category');
Route::post('/admin/category/{id}/update', [AdminController::class, 'updateCategory'])->name('update.category')->middleware('can:update,App\Model\Category');
Route::post('/admin/category/delete', [AdminController::class, 'deleteCategory'])->name('delete.category')->middleware('can:delete,App\Model\Category');

Route::get('/admin/item', [AdminController::class, 'adminItems'])->name('manage.items')->middleware('can:viewAny,App\Model\Item');
Route::get('admin/item/new', [AdminController::class, 'newItem'])->name('new.item')->middleware('can:create,App\Model\Item');
Route::post('admin/item/store', [AdminController::class, 'storeNewItem'])->name('store.item')->middleware('can:create,App\Model\Item');
Route::get('admin/item/{id}/edit', [AdminController::class, 'editItem'])->name('edit.item')->middleware('can:update,App\Model\Item');
Route::post('admin/item/{id}/update', [AdminController::class, 'updateItem'])->name('update.item')->middleware('can:update,App\Model\Item');
Route::post('/admin/item/delete', [AdminController::class, 'deleteItem'])->name('delete.item')->middleware('can:delete,App\Model\Item');
Route::post('/admin/item/{id}/restore', [AdminController::class, 'restoreItem'])->name('restore.item')->middleware('can:restore,App\Model\Item');

Route::get('/admin/manage', [AdminController::class, 'adminOrders'])->name('manage.orders')->middleware('can:viewAny,App\Model\Order');
Route::get('/admin/manage/received', [AdminController::class, 'viewReceived'])->name('manage.received')->middleware('can:viewAny,App\Model\Order');
Route::get('/admin/manage/processed', [AdminController::class, 'viewProcessed'])->name('manage.processed')->middleware('can:viewAny,App\Model\Order');
Route::post('/admin/manage/accept/{id}', [AdminController::class, 'acceptOrder'])->name('accept.order')->middleware('can:process,App\Model\Order');
Route::post('/admin/manage/reject/{id}', [AdminController::class, 'rejectOrder'])->name('reject.order')->middleware('can:process,App\Model\Order');

Route::get('/admin/{type}/{id}/confirm-delete', [AdminController::class, 'deteleConfirmation'])->name('confirm.delete');

